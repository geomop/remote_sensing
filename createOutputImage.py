# -*- coding: utf-8 -*-
"""
Created on Tue May 15 20:30:26 2018

@author: tunabalik
"""

def createOutputImage(outFilename, inDataset):
    # Define the image driver to be used 
    # This defines the output file format (e.g., GeoTiff)
    driver = gdal.GetDriverByName( "GTiff" )
    # Check that this driver can create a new file.
    metadata = driver.GetMetadata()
    
    # Get the spatial information from the input file
    geoTransform = inDataset.GetGeoTransform()
    geoProjection = inDataset.GetProjection()
    # Create an output file of the same size as the inputted 
    # image but with only 1 output image band.
    newDataset = driver.Create(outFilename, inDataset.RasterXSize, \
    inDataset.RasterYSize, 1, gdal.GDT_Float32)
    # Define the spatial information for the new image.
    newDataset.SetGeoTransform(geoTransform)
    newDataset.SetProjection(geoProjection)
    return newDataset