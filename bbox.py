# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 16:11:01 2018

@author: tunabalik
"""

def bbox (shpDirectory, shpName):
    
    hw=shpDirectory+shpName

    sf = shapefile.Reader(hw)
    shapes = sf.shapes()

    # get the SITE_ID (third column)
    site_ID=list()
    shpList=list(sf.iterShapeRecords())
    for i in range(len(shpList)):
        site_ID.append(shpList[i].record[2])
    
    # get the HW_ID (second column)
    hw_ID=list()
    for i in range(len(shpList)):
        hw_ID.append(shpList[i].record[1])
        
# CHECK THIS AGAIN **************************
    tt=np.argsort(site_ID)
    qq=np.array(site_ID)
    zz=qq[tt]
    rr=np.array(hw_ID)
# CHECK THIS AGAIN **************************
    

    #site ID list
    siteIDlist=np.unique(zz)
    newShapeList=[ shpList[i] for i in tt]
    L = [list(v) for k,v in itertools.groupby(zz)]
    
    #Create a dictionary for sites and HW
    tupleSiteHw=zip(site_ID,rr)
    dictSiteHW = defaultdict(list)
    for site, hw_site in tupleSiteHw:
        dictSiteHW[site].append(hw_site)

    Lbox=list()
    Lind=list()
    for i in range(len(L)):
        bnbox = []
        numVertices=[]
        a=len(L[i])
        subShapeList=newShapeList[0:a]
        for j in range(len(subShapeList)):
            bnbox.append(subShapeList[j].shape.bbox)
            numVertices.append(subShapeList[j].shape.points)
        np_bnbox=np.array(bnbox)
        
        # Creating the limits of the bounding box
        max_bnbox=np.max(np_bnbox, axis=0)
        east=max_bnbox[2]
        north=max_bnbox[3]
        min_bnbox=np.min(np_bnbox, axis=0)
        west=min_bnbox[0]
        south=min_bnbox[1]
        Lbox.append([east,west,north,south])
        
        flattenedVertices = [val for sublist in numVertices for val in sublist]
        Lind.append(len(flattenedVertices))
        newShapeList=newShapeList[a:]
        
    return Lbox, Lind, dictSiteHW, siteIDlist
    