# -*- coding: utf-8 -*-
"""
Created on Tue May 15 20:35:18 2018

@author: tunabalik
"""
# global variable in order to run the function

def calcIndex(bandDirectory, outFilePath, indList, sensorType):
        
    # Open the bands dataset
    bandFilename = [f for f in os.listdir(bandDirectory) if f.endswith('.' + 'tif') or f.endswith('.' + 'jp2') ] 
    bandFilename.sort()
    
    dataset=list()
    for j in range(len(bandFilename)):    
        dataset.append(gdal.Open( bandDirectory+'/'+bandFilename[j], gdal.GA_ReadOnly ))

     
    for t in range(len(indList)):
        # here assume rgb has the highest spatial resolution
        vars()['outDataset'+indList[t]]=createOutputImage(outFilePath+indList[t]+'.tif', dataset[0])
        vars()['outputLine'+indList[t]]=b''
    

     
    # SENSOR TYPES 
    if sensorType=='LandsatTM':
        blue_band = dataset[0]        
        green_band = dataset[1]        
        red_band = dataset[2]
        nir_band = dataset[3]
        swir1_band = dataset[4]
        # no thermal band on reflectance
        swir2_band = dataset[5]
        
    
    if sensorType=='LandsatETM':
        blue_band = dataset[0]        
        green_band = dataset[1]        
        red_band = dataset[2]
        nir_band = dataset[3]
        swir1_band = dataset[4]
        # no thermal band on reflectance
        swir2_band = dataset[5]
        # no pan band on reflectance
             


    if sensorType=='LandsatOLI':
        ultrablue_band = dataset[0]        
        blue_band = dataset[1]        
        green_band = dataset[2]
        red_band = dataset[3]
        nir_band = dataset[4]
        # no thermal band on reflectance
        swir1_band = dataset[5]        
        swir2_band = dataset[6]
        # no pan band on reflectance
        cirrus_band = dataset[7]
        tirs1_band = dataset[8]
        tirs2_band = dataset[9]


    if sensorType=='Sentinel2A': 
        overview_band = dataset[0]        
        blue_band = dataset[1]        
        green_band = dataset[2]
        red_band = dataset[3]
        nir_band = dataset[4]
        nir705_band = dataset[5]        
        nir740_band = dataset[6]
        nir783_band = dataset[7]
        nir865_band = dataset[8]
        nir940_band = dataset[9]
        swir1375_band = dataset[10]
        swir1_band = dataset[11]
        swir2_band = dataset[12]        


    if sensorType=='Sentinel2B': 
        overview_band = dataset[0]        
        blue_band = dataset[1]        
        green_band = dataset[2]
        red_band = dataset[3]
        nir_band = dataset[4]
        nir705_band = dataset[5]        
        nir740_band = dataset[6]
        nir783_band = dataset[7]
        nir865_band = dataset[8]
        nir940_band = dataset[9]
        swir1375_band = dataset[10]
        swir1_band = dataset[11]
        swir2_band = dataset[12]
        TCI_band = dataset[13]         
        
        
        
    #upsampling rasters for pixel matching to the highest resolution
    setYsize=list()
    for p in range(len(dataset)):
        bandname=dataset[p]
        Ysize=bandname.RasterYSize
        setYsize.append(Ysize)
    multp=[int(max(setYsize)/item) for item in setYsize]
    
    for p in range(len(dataset)):
        if multp[p] != 1.0:
            caseData=dataset[p]
            correctCoeff=multp[p]
            dataset[p]=createResampleImage(caseData, correctCoeff)
            
# If the image is ever needed             
#            driver2= gdal.GetDriverByName ('GTiff')
#            writeRaster=driver2.CreateCopy('/home/tunabalik/Desktop/tests.tif',dataset[p],0)
#            writeRaster=None
            
    # Retrieve the number of lines within the image
    numLines = red_band.RasterYSize

    for line in range(numLines):
         
            
        # Read data for the current line
        blue_scanline = blue_band.ReadRaster( 0, line, blue_band.RasterXSize, 1, blue_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        blue_tuple = struct.unpack('f' * blue_band.RasterXSize, blue_scanline)            
    
        # Read data for the current line
        green_scanline = green_band.ReadRaster( 0, line, green_band.RasterXSize, 1, green_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        green_tuple = struct.unpack('f' * green_band.RasterXSize, green_scanline)            
        
        
        # Read data for the current line
        red_scanline = red_band.ReadRaster( 0, line, red_band.RasterXSize, 1, red_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        red_tuple = struct.unpack('f' * red_band.RasterXSize, red_scanline)
        
        # Read data for the current line
        nir_scanline = nir_band.ReadRaster( 0, line, nir_band.RasterXSize, 1, nir_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        nir_tuple = struct.unpack('f' * nir_band.RasterXSize, nir_scanline)
        
        # Read data for the current line
        swir1_scanline = swir1_band.ReadRaster( 0, line, swir1_band.RasterXSize, 1, swir1_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        swir1_tuple = struct.unpack('f' * swir1_band.RasterXSize, swir1_scanline)
        
        # Read data for the current line
        swir2_scanline = swir2_band.ReadRaster( 0, line, swir2_band.RasterXSize, 1, swir2_band.RasterXSize, 1, gdal.GDT_Float32 )
        # Unpack data
        swir2_tuple = struct.unpack('f' * swir2_band.RasterXSize, swir2_scanline)

        if sensorType=='LandsatETM':
            # Read data for the current line
            pan_scanline = pan_band.ReadRaster( 0, line, pan_band.RasterXSize, 1, pan_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            pan_tuple = struct.unpack('f' * pan_band.RasterXSize, pan_scanline)
        
        if sensorType=='LandsatOLI':            
            # Read data for the current line
            ultrablue_scanline = ultrablue_band.ReadRaster( 0, line, ultrablue_band.RasterXSize, 1, ultrablue_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            ultrablue_tuple = struct.unpack('f' * ultrablue_band.RasterXSize, ultrablue_scanline)
            # Read data for the current line
            pan_scanline = pan_band.ReadRaster( 0, line, pan_band.RasterXSize, 1, pan_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            pan_tuple = struct.unpack('f' * pan_band.RasterXSize, pan_scanline)
            # Read data for the current line
            cirrus_scanline = cirrus_band.ReadRaster( 0, line, cirrus_band.RasterXSize, 1, cirrus_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            cirrus_tuple = struct.unpack('f' * cirrus_band.RasterXSize, cirrus_scanline)
            # Read data for the current line
            tirs1_scanline = tirs1_band.ReadRaster( 0, line, tirs1_band.RasterXSize, 1, tirs1_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            tirs1_tuple = struct.unpack('f' * tirs1_band.RasterXSize, tirs1_scanline)
            # Read data for the current line
            tirs2_scanline = tirs2_band.ReadRaster( 0, line, tirs2_band.RasterXSize, 1, tirs2_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            tirs2_tuple = struct.unpack('f' * tirs2_band.RasterXSize, tirs2_scanline)
        
        if sensorType=='Sentinel2A':

            # Read data for the current line
            overview_scanline = overview_band.ReadRaster( 0, line, overview_band.RasterXSize, 1, overview_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            overview_tuple = struct.unpack('f' * overview_band.RasterXSize, overview_scanline)
            # Read data for the current line
            nir705_scanline = nir705_band.ReadRaster( 0, line, nir705_band.RasterXSize, 1, nir705_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir705_tuple = struct.unpack('f' * nir705_band.RasterXSize, nir705_scanline)
            # Read data for the current line
            nir740_scanline = nir740_band.ReadRaster( 0, line, nir740_band.RasterXSize, 1, nir740_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir740_tuple = struct.unpack('f' * nir740_band.RasterXSize, nir740_scanline)
            # Read data for the current line
            nir783_scanline = nir783_band.ReadRaster( 0, line, nir783_band.RasterXSize, 1, nir783_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir783_tuple = struct.unpack('f' * nir783_band.RasterXSize, nir783_scanline)
            # Read data for the current line
            nir865_scanline = nir865_band.ReadRaster( 0, line, nir865_band.RasterXSize, 1, nir865_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir865_tuple = struct.unpack('f' * nir865_band.RasterXSize, nir865_scanline)
            # Read data for the current line
            nir940_scanline = nir940_band.ReadRaster( 0, line, nir940_band.RasterXSize, 1, nir940_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir940_tuple = struct.unpack('f' * nir940_band.RasterXSize, nir940_scanline)
            # Read data for the current line
            swir1375_scanline = swir1375_band.ReadRaster( 0, line, swir1375_band.RasterXSize, 1, swir1375_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            swir1375_tuple = struct.unpack('f' * swir1375_band.RasterXSize, swir1375_scanline)
            
        if sensorType=='Sentinel2B':

            # Read data for the current line
            overview_scanline = overview_band.ReadRaster( 0, line, overview_band.RasterXSize, 1, overview_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            overview_tuple = struct.unpack('f' * overview_band.RasterXSize, overview_scanline)
            # Read data for the current line
            nir705_scanline = nir705_band.ReadRaster( 0, line, nir705_band.RasterXSize, 1, nir705_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir705_tuple = struct.unpack('f' * nir705_band.RasterXSize, nir705_scanline)
            # Read data for the current line
            nir740_scanline = nir740_band.ReadRaster( 0, line, nir740_band.RasterXSize, 1, nir740_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir740_tuple = struct.unpack('f' * nir740_band.RasterXSize, nir740_scanline)
            # Read data for the current line
            nir783_scanline = nir783_band.ReadRaster( 0, line, nir783_band.RasterXSize, 1, nir783_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir783_tuple = struct.unpack('f' * nir783_band.RasterXSize, nir783_scanline)
            # Read data for the current line
            nir865_scanline = nir865_band.ReadRaster( 0, line, nir865_band.RasterXSize, 1, nir865_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir865_tuple = struct.unpack('f' * nir865_band.RasterXSize, nir865_scanline)
            # Read data for the current line
            nir940_scanline = nir940_band.ReadRaster( 0, line, nir940_band.RasterXSize, 1, nir940_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            nir940_tuple = struct.unpack('f' * nir940_band.RasterXSize, nir940_scanline)
            # Read data for the current line
            swir1375_scanline = swir1375_band.ReadRaster( 0, line, swir1375_band.RasterXSize, 1, swir1375_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            swir1375_tuple = struct.unpack('f' * swir1375_band.RasterXSize, swir1375_scanline)
            # Read data for the current line
            TCI_scanline = TCI_band.ReadRaster( 0, line, TCI_band.RasterXSize, 1, TCI_band.RasterXSize, 1, gdal.GDT_Float32 )
            # Unpack data
            TCI_tuple = struct.unpack('f' * TCI_band.RasterXSize, TCI_scanline)


    

            # Define variable for output line 
        for k in range(len(indList)):
            vars()['outputLine'+indList[k]]=b''

        # Loop through the columns within the image
   
            for i in range(len(red_tuple)):
                
                # ARVI: Atmospherically Resistant Vegetation Index
                gam=1                
                R1arvi = nir_tuple[i] - (red_tuple[i]-gam*(blue_tuple[i]-red_tuple[i]))
                R2arvi = nir_tuple[i] + (red_tuple[i]-gam*(blue_tuple[i]-red_tuple[i]))
                if R2arvi == 0:
                    arvi = 0
                else:
                    arvi = R1arvi/R2arvi
                       
                outputLineARVI = outputLineARVI + struct.pack('f', arvi)
                del arvi
                 
                # CVI: Chlorophyll Vegetation Index
                R1cvi = red_tuple[i]*nir_tuple[i]
                R2cvi = green_tuple[i]*green_tuple[i]
                if R2cvi == 0:
                    cvi = 0
                else:
                    cvi = R1cvi/R2cvi 

                outputLineCVI = outputLineCVI + struct.pack('f', cvi)
                del cvi
                          
                # DVI: Difference Vegetation Index
                dvi = (nir_tuple[i] - red_tuple[i])
                # Add the current pixel to the output line
               
                outputLineDVI = outputLineDVI + struct.pack('f', dvi)
                del dvi
                    
                    
                # DWSI: Disease Water Stress Index            
                R1dwsi = nir_tuple[i] + green_tuple[i]
                R2dwsi = swir2_tuple[i] + red_tuple[i]
                if R2dwsi == 0:
                    dwsi = 0
                else:
                    dwsi = R1dwsi/R2dwsi
              
                outputLineDWSI = outputLineDWSI + struct.pack('f', dwsi)
                    
                    
                # DWSI1: Disease Water Stress Index - 1            
                if swir2_tuple[i] == 0:
                    dwsi1 = 0
                else:
                    dwsi1 = nir_tuple[i] / swir2_tuple[i]

                outputLineDWSI1 = outputLineDWSI1 + struct.pack('f', dwsi1)             
                
                    
                # DWSI2: Disease Water Stress Index - 2            
                dwsi2 = swir2_tuple[i] / green_tuple[i]
                           
                outputLineDWSI2 = outputLineDWSI2 + struct.pack('f', dwsi2)                
                    
                    
                # DWSI3: Disease Water Stress Index - 3            
                dwsi3 = swir2_tuple[i] / red_tuple[i]
                              
                outputLineDWSI3 = outputLineDWSI3 + struct.pack('f', dwsi3)                
                    
                    
                # DWSI4: Disease Water Stress Index - 4            
                dwsi4 = green_tuple[i] / red_tuple[i]
                                
                outputLineDWSI4 = outputLineDWSI4 + struct.pack('f', dwsi4)
    
                    
                # DWSI5: Disease Water Stress Index - 5           
                R1dwsi5 = nir_tuple[i] - green_tuple[i]
                R2dwsi5 = swir2_tuple[i] + red_tuple[i]
                if R2dwsi5 == 0:
                    dwsi5 = 0
                else:
                    dwsi5 = R1dwsi5/R2dwsi5 
                
                outputLineDWSI5 = outputLineDWSI5 + struct.pack('f', dwsi5)                
                    
                    
                # EVI: Enhanced Vegetation Index           
                R1evi = nir_tuple[i] - red_tuple[i]
                R2evi = nir_tuple[i] + 6*red_tuple[i] - 7.5*blue_tuple[i] +1
                evi = 0
                if R2evi == 0:
                    evi = 0
                else:
                    evi = 2.5 * R1evi / R2evi
               
                outputLineEVI = outputLineEVI + struct.pack('f', evi)
                
                    
                # GEMI: Global Environmental Monitoring Index           
                etaR1 = 2 * (nir_tuple[i]*nir_tuple[i]-red_tuple[i]*red_tuple[i]) + 1.5*nir_tuple[i] + 0.5*red_tuple[i]
                etaR2 = nir_tuple[i] + red_tuple[i] + 0.5
                eta = etaR1 / etaR2
                Comp1 = eta * (1-0.25 *eta)
                R1gemi = red_tuple[i] - 0.125
                R2gemi = 1 - red_tuple[i]
                if R2gemi == 0:
                    gemi = 0
                else:
                    gemi = Comp1-R1gemi/R2gemi
                
                outputLineGEMI = outputLineGEMI + struct.pack('f', gemi)
                    
                    
                # GARI: Green Atmospherically Resistant Index          
                gam=1.7
                R1gari = nir_tuple[i] - (green_tuple[i] - gam*(blue_tuple[i]-red_tuple[i])) 
                R2gari = nir_tuple[i] + (green_tuple[i] - gam*(blue_tuple[i]-red_tuple[i]))
                if R2gari == 0:
                    gari = 0
                else: 
                    gari = R1gari / R2gari 
              
                outputLineGARI = outputLineGARI + struct.pack('f', gari)
                
                    
                # GDVI: Green Difference Vegetation Index           
                gdvi = nir_tuple[i] - green_tuple[i]
                            
                outputLineGDVI = outputLineGDVI + struct.pack('f', gdvi)
                
                    
                # GNDVI: Green Normalized Difference Vegetation Index           
                R1gndvi = nir_tuple[i] - green_tuple[i]
                R2gndvi = nir_tuple[i] + green_tuple[i]
                if R2gndvi == 0:
                    gndvi = 0
                else: 
                    gndvi = R1gndvi / R2gndvi
               
                outputLineGNDVI = outputLineGNDVI + struct.pack('f', gndvi)
                    
                    
                # GRVI: Green Ratio Vegetation Index
                grvi = nir_tuple[i] / green_tuple[i]
                            
                outputLineGRVI = outputLineGRVI + struct.pack('f', grvi)
                
                    
                # GVI: Green Vegetation Index
                gvi = (-0.2848)* blue_tuple[i]+ (-0.2435)*green_tuple[i]+(-0.5436)*red_tuple[i]+(0.7243)*nir_tuple[i]+(0.084)*swir1_tuple[i]+(-0.18)*swir2_tuple[i]  
                              
                outputLineGVI = outputLineGVI + struct.pack('f', gvi)
                
                    
                # MCARI1: Modified Chlorophyll Absorption in Reflectance Index 1
                mcari1 = 1.2*(2.5*(nir_tuple[i]-red_tuple[i])-1.3*(nir_tuple[i]-green_tuple[i]))
                              
                outputLineMCARI1 = outputLineMCARI1 + struct.pack('f', mcari1)
                    
                    
                # MNLI: Modified Non-Linear Index         
                L=0.5
                R1mnli = (nir_tuple[i]*nir_tuple[i]-red_tuple[i])*(1+L)
                R2mnli = nir_tuple[i]*nir_tuple[i]+red_tuple[i]+L
                if R2mnli == 0:
                    mnli = 0
                else:
                    mnli = R1mnli / R2mnli
                
                outputLineMNLI = outputLineMNLI + struct.pack('f', mnli)
                   
                   
                # MVI: Mid-Infrared Vegetation Index               
                mvi=nir_tuple[i]/swir1_tuple[i]
                               
                outputLineMVI = outputLineMVI + struct.pack('f', mvi)
                
                    
                # MSI: Moisture Stress Index
                msi = swir2_tuple[i]/nir_tuple[i]
                              
                outputLineMSI = outputLineMSI + struct.pack('f', msi)
                    
                    
                # NDVI: Normalized Difference Vegetation Index
                R1ndvi = nir_tuple[i] - red_tuple[i]
                R2ndvi = nir_tuple[i] + red_tuple[i]
                ndvi = 0
                if R2ndvi == 0:
                    ndvi = 0
                else:
                    ndvi = R1ndvi/R2ndvi
               
                outputLineNDVI = outputLineNDVI + struct.pack('f', ndvi)
                
                
                # NDWI: Normalized Difference Water Index
                R1ndwi = green_tuple[i] - nir_tuple[i]
                R2ndwi = green_tuple[i] + nir_tuple[i]
                ndwi = 0
                if R2ndvi == 0:
                    ndwi = 0
                else:
                    ndwi = R1ndwi/R2ndwi
               
                outputLineNDWI = outputLineNDWI + struct.pack('f', ndwi)
                
                    
                # NLI: Non-Linear Index           
                R1nli = nir_tuple[i]*nir_tuple[i]-red_tuple[i]
                R2nli = nir_tuple[i]*nir_tuple[i]+red_tuple[i]
                if R2nli == 0:
                    nli = 0
                else:
                    nli = R1nli / R2nli
                
                outputLineNLI = outputLineNLI + struct.pack('f', nli)
                    
                    
                # NDMI: Normalized Difference 820/1600 Moisture Index           
                R1ndmi = nir_tuple[i]-swir1_tuple[i]
                R2ndmi = nir_tuple[i]+swir1_tuple[i]
                if R2ndmi == 0:
                    ndmi = 0
                else: 
                    ndmi = R1ndmi / R2ndmi
                
                outputLineNDMI = outputLineNDMI + struct.pack('f', nli)
                    
                    
                # OSAVI: Optimized Soil Adjusted Vegetation Index          
                R1osavi = 1.5*(nir_tuple[i]-red_tuple[i])
                R2osavi = nir_tuple[i]+red_tuple[i]+0.16
                if R2osavi == 0:
                    osavi = 0
                else:
                    osavi = R1osavi / R2osavi
                
                outputLineOSAVI = outputLineOSAVI + struct.pack('f', osavi)
                
                    
                # SAVI:  Soil Adjusted Vegetation Index          
                R1savi = 1.5*(nir_tuple[i]-red_tuple[i])
                R2savi = nir_tuple[i]+red_tuple[i]+0.5
                if R2savi == 0:
                    savi = 0
                else:
                    savi = R1osavi / R2osavi
                
                outputLineSAVI = outputLineSAVI + struct.pack('f', savi)
                    
                    
                # SIWSI: Normalized Difference 860/1640       
                R1siwsi = nir_tuple[i]-swir2_tuple[i]
                R2siwsi = nir_tuple[i]+swir2_tuple[i]
                if R2siwsi == 0:
                    siwsi = 0
                else:
                    siwsi = R1siwsi / R2siwsi
                
                outputLineSIWSI = outputLineSIWSI + struct.pack('f', siwsi)
                
                # SR: Simple Ratio
                sr = nir_tuple[i]/red_tuple[i]
                              
                outputLineSR = outputLineSR + struct.pack('f', sr)
                    
                # VARI: Visible Atmospherically Resistant Index       
                R1vari = green_tuple[i]-red_tuple[i]
                R2vari = green_tuple[i]+red_tuple[i]-blue_tuple[i]
                if R2vari == 0:
                    vari = 0
                else:
                    vari = R1vari / R2vari
                
                outputLineVARI = outputLineVARI + struct.pack('f', vari)
                    
                # WET: Wetness 
                wet = 0.1509*blue_tuple[i]+0.1973*green_tuple[i]+0.3279*red_tuple[i]+0.3406*nir_tuple[i]-0.7112*swir1_tuple[i]-0.4572*swir2_tuple[i] 
                               
                outputLineWET = outputLineWET + struct.pack('f', wet)
                
            openLine=vars()['outDataset'+indList[k]]
            openBand=openLine.GetRasterBand(1)
    
            openBand.WriteRaster(0, line, red_band.RasterXSize, 1, vars()['outputLine'+indList[k]], buf_xsize=red_band.RasterXSize, buf_ysize=1, buf_type=gdal.GDT_Float32)
            vars()['outDataset'+indList[k]].FlushCache()    

                
#    duration=5
#    freq=420     
#    os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))  
        
