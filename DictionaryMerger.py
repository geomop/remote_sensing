# -*- coding: utf-8 -*-
"""
Created on Sun Apr 29 12:47:06 2018

@author: tunabalik
"""
def DictionaryMerger (dictIndices,dictSiteHW):

    dictHW = defaultdict(list)

    d1=dictIndices
    d2=dictSiteHW


    k1=list(d1.keys())
    v1=list(d1.values())


    k2=list(d2.keys())
    v2=list(d2.values())
    vals=np.column_stack([k2,v2])

    for i in range(len(k1)):
        HWfull=list()
        IndSite=d1[k1[i]]
        for j in IndSite:
            tempInd=k2.index(j)
            HWfull.append(v2[tempInd])
            HWfull2 = [val for sublist in HWfull for val in sublist]
            dictHW[k1[i]]=HWfull2

    return dictHW
        
        