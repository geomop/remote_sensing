# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 11:54:39 2018

@author: tunabalik
"""

def ReadProfiles (hw,right,left): 

    # read the HW shapefile
    sf = shapefile.Reader(hw)
    shapes = sf.shapes()

    # read the Right offset shapefile
    sf_R = shapefile.Reader(right)
    shapes_R = sf_R.shapes()

    # read the Left offset shapefile
    sf_L = shapefile.Reader(left)
    shapes_L = sf_L.shapes()

    # get the HW_ID (second column)
    HW_ID=list()
    shpList=list(sf.iterShapeRecords())
    for i in range(len(shpList)):
        HW_ID.append(shpList[i].record[1])
        
    # get vertices from the HW
    HW_pnts = []
    for i in range(len(shapes)):
        HW_pnts.append(shapes[i].points)

    # get vertices from the offset R and offset L
    offsetR_pnts = []
    offsetL_pnts = []
    for i in range(len(shapes_R)):
        offsetR_pnts.append(shapes_R[i].points)
        offsetL_pnts.append(shapes_L[i].points)
    
    return HW_pnts, offsetR_pnts, offsetL_pnts, shapes, HW_ID