# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 11:09:53 2018

@author: tunabalik
"""

def RandomPoints (Lbox,Lind):
    
    # this guarantees two samples do not fall in the same pixel 
    radius = 45    
    
    Lrand = list()
    for i in range(len(Lbox)):
        b1=Lbox[i]
        p1=Lind[i]
        rangeX = (int(b1[1]-150), int(b1[0]+150))
        rangeY = (int(b1[3]-150), int(b1[2]+150))
        
        deltas = set()
        for x in range(-radius, radius+1):
            for y in range(-radius, radius+1):
                if x*x + y*y <= radius*radius:
                    deltas.add((x,y))
        
        randPoints = []
        excluded = set()
        i = 0
        
        while i<p1:
            x = random.randrange(*rangeX)
            y = random.randrange(*rangeY)
            if (x,y) in excluded: continue
            randPoints.append((x,y))
            i += 1
            excluded.update((x+dx, y+dy) for (dx,dy) in deltas)
        
        Lrand.append(randPoints)
        
    return Lrand
        
    