# -*- coding: utf-8 -*-
"""
Created on Tue May 15 20:30:26 2018

@author: tunabalik
"""

def createResampleImage(caseData, correctCoeff):
    # Define the image driver to be used 
    # This defines the output file format (e.g., GeoTiff)

    driver = gdal.GetDriverByName( 'MEM' )
    # Check that this driver can create a new file.
    metadata = driver.GetMetadata()
    
    # Get the spatial information from the input file
    geoTransform = caseData.GetGeoTransform()
    geoProjection = caseData.GetProjection()

    # Define the spatial information for the new image.
    geotrans=list(geoTransform)
    geotrans[1] /= correctCoeff
    geotrans[5] /= correctCoeff
    
    newDataset = driver.Create('', int(caseData.RasterXSize), \
    int(caseData.RasterYSize), 1, gdal.GDT_Float32)

    
    newDataset.SetGeoTransform(tuple(geotrans))
    newDataset.SetProjection(geoProjection)
    gdal.ReprojectImage(caseData,newDataset,'None','None',gdal.GRA_Bilinear)
    

    return newDataset