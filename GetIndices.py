def GetIndices (rstFilename,pnts):         
    

# read the raster

    dataset = gdal.Open( rstFilename, gdal.GA_ReadOnly )
    gt=dataset.GetGeoTransform()
    gt=np.array(gt)
    spIndex = dataset.GetRasterBand(1)
    #spIndex=np.array(dataset.GetRasterBand(1).ReadAsArray())

    x=[]

# get values for vertices
    
      
    # prepare arrays
    mx = []
    my = []
    mx = [item[0] for item in pnts]
    my = [item[1] for item in pnts]
    a_mx=np.array(mx)
    a_my=np.array(my)
    
       
        #Convert from map to pixel coordinates.
        #Only works for geotransforms with no rotation.
        # CAREFUL HERE: gdal and shapefile modules have nonmatching xy,rowcol
    py = np.int_((a_mx-gt[0])/gt[1])
    px = np.int_((a_my-gt[3])/gt[5]) 
    IndexVals=spIndex[px,py]
    
   
    
    y=[a_mx,a_my,IndexVals]
    x.append(y)
        
        
    del IndexVals, px, py
    return x