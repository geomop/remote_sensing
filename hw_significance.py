
def hw_significance (shpDirectory, indDirectory, bufPointsName, hwPointsName, alpha, std_coef):
    
    sortedInd=os.listdir(indDirectory)
    sortedInd.sort()    
    
    bfPoints=shpDirectory+bufPointsName
    hwPoints=shpDirectory+hwPointsName

    
    sf = shapefile.Reader(bfPoints)
    shapesBUFpoints = sf.shapes()
    del sf
    
    sf = shapefile.Reader(bfPoints)
    shapesBUFrecords = sf.records()
    del sf
    
    sf = shapefile.Reader(hwPoints)
    shapesHWpoints = sf.shapes()
    del sf
    
    sf = shapefile.Reader(hwPoints)
    shapesHWrecords = sf.records()
    del sf

    # Read hw sample points from the line
    HW_pnts = []
    for i in range(len(shapesHWpoints)):
        HW_pnts.append(shapesHWpoints[i].points)


    # Read random points in the buffer
    buf_pnts = []
    for i in range(len(shapesBUFpoints)):
        buf_pnts.append(shapesBUFpoints[i].points)
        

    # Unnest list
    buf_pnts = [val for sublist in buf_pnts for val in sublist]
    HW_pnts = [val for sublist in HW_pnts for val in sublist]
    
    #unnest site ids
    shapesBUFrecords = [val for sublist in shapesBUFrecords for val in sublist]

    
    # merging with sites
    siteArray = np.asarray(shapesBUFrecords)
    pointArray = np.asarray(buf_pnts)
    pointArray=np.squeeze(pointArray)
    a1=[siteArray,pointArray]
    
    t=a1[0]
    idx = np.nonzero(np.diff(t))[0] + 1
    idx = np.insert(idx, 0, 0)
    
    keepind=[]
    for j in range(len(idx)-1):
        keepind.append([idx[j],idx[j+1]-1])
        
    keepind.insert(len(keepind),[idx[len(idx)-1],len(siteArray)])
   


    #cluster site ids CREATE DICTIONARIES
    clusterSite=[list(j) for i, j in groupby(a1[0])]

    dict_sitesOUT=dict()
    dict_sitesIN=dict()
    #start creating hollow way dictionary
    for j in range(len(clusterSite)):
        dict_sitesOUT[clusterSite[j][0]] = buf_pnts[keepind[j][0]:keepind[j][1]]
        dict_sitesIN[clusterSite[j][0]] = HW_pnts[keepind[j][0]:keepind[j][1]]
        

    # Get index values for random points and Hollow Ways
    siteList=np.unique(siteArray)

    
    
    
    dict_sitesOUTind=dict()
    dict_sitesINind=dict()
    
    for rstFilename in sortedInd:
        for k in range(len(siteList)):
            xHWIndices=[]
            xRandIndices=[]

        
            # if there is an xml in the folder, but it doesn't work for tif.xml
            if rstFilename.endswith(".tif"):

                xRandIndices = GetIndices (indDirectory+rstFilename,dict_sitesOUT[siteList[k]])  
                xHWIndices = GetIndices (indDirectory+rstFilename,dict_sitesIN[siteList[k]])

            dict_sitesOUTind[siteList[k]]= xRandIndices[0][2]
            dict_sitesINind[siteList[k]]=  xHWIndices[0][2]
            
        dict_sitesP=dict()
        for k in range(len(siteList)):
            a=dict_sitesINind[siteList[k]]
            b=dict_sitesOUTind[siteList[k]]
            
            std_a=np.std(a, ddof=1)
            avg_a=np.average(a)
            up_a=avg_a+std_a*std_coef
            down_a=avg_a-std_a*std_coef

            std_b=np.std(b, ddof=1)
            avg_b=np.average(b)
            up_b=avg_b+std_b*std_coef
            down_b=avg_b-std_b*std_coef
      
            #write to a good array        
            thr_a=[i for i,x in enumerate(a) if up_a > x > down_a]
            good_a=[ a[i] for i in thr_a ]
    
            thr_b=[i for i,x in enumerate(b) if up_b > x > down_b]
            good_b=[ b[i] for i in thr_b ]
   
            t_result=stats.ttest_ind(good_a,good_b,equal_var=False)
            dict_sitesP[siteList[k]]=t_result.pvalue
            

    
    return goodStatsSorted, goodStatsInd
    
    
