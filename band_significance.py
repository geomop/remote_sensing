
def band_significance (shpDirectory, indDirectory, bufPointsName, hwPointsName, alpha, std_coef):
    
    sortedInd=os.listdir(indDirectory)
    sortedInd.sort()    
    
    bfPoints=shpDirectory+bufPointsName
    hwPoints=shpDirectory+hwPointsName

    
    sf = shapefile.Reader(bfPoints)
    shapesBUFpoints = sf.shapes()
    del sf
    
    sf = shapefile.Reader(hwPoints)
    shapesHWpoints = sf.shapes()
    del sf

    # Read hw sample points from the line
    HW_pnts = []
    for i in range(len(shapesHWpoints)):
        HW_pnts.append(shapesHWpoints[i].points)


    # Read random points in the buffer
    buf_pnts = []
    for i in range(len(shapesBUFpoints)):
        buf_pnts.append(shapesBUFpoints[i].points)

    # Unnest list
    buf_pnts = [val for sublist in buf_pnts for val in sublist]
    HW_pnts = [val for sublist in HW_pnts for val in sublist]

    # Get index values for random points and Hollow Ways

    xHWIndicesSet=list() 
    xRandIndicesSet=list()
    
    for rstFilename in sortedInd:

        xRandIndices=list()
        xHWIndices=list()
        
        if rstFilename.endswith(".tif"):

            xRandIndices = GetIndices (indDirectory+rstFilename,buf_pnts)  
            xHWIndices = GetIndices (indDirectory+rstFilename,HW_pnts)

            
        xRandIndicesSet.append(xRandIndices)
        xHWIndicesSet.append(xHWIndices)
        xRandIndices=[]
        xHWIndices=[]

    # Statisical comparison
    tpass=list()
    tstats=list()

    for k in range(len(xRandIndicesSet)):
        tempind=xRandIndicesSet[k]
        temphws=xHWIndicesSet[k]
        a=tempind[0][2]
        b=temphws[0][2]
        
        # get array descriptors
                
        std_a=np.std(a, ddof=1)
        avg_a=np.average(a)
        up_a=avg_a+std_a*std_coef
        down_a=avg_a-std_a*std_coef

        std_b=np.std(b, ddof=1)
        avg_b=np.average(b)
        up_b=avg_b+std_b*std_coef
        down_b=avg_b-std_b*std_coef
      
        #write to a good array        
        thr_a=[i for i,x in enumerate(a) if up_a > x > down_a]
        good_a=[ a[i] for i in thr_a ]
    
        thr_b=[i for i,x in enumerate(b) if up_b > x > down_b]
        good_b=[ b[i] for i in thr_b ]
   
        t_result=stats.ttest_ind(good_a,good_b,equal_var=False)
#        t_result=stats.mannwhitneyu(good_a,good_b, alternative='two-sided')
        if t_result.pvalue< (alpha/2):
            flagSite=1
        else:
            flagSite=0
        tpass.append(flagSite)
        tstats.append(t_result.pvalue)
    
    goodList=[i for i,x in enumerate(tpass) if x == 1]
    
    goodInd=[ sortedInd[i] for i in goodList ]   
    goodStats= [ tstats[i] for i in goodList ] 
    


    statInd=np.argsort(goodStats)
    goodStatsSorted=[ goodStats[i] for i in statInd ]
    goodStatsInd=[ goodInd[i] for i in statInd ]
    
    return goodStatsSorted, goodStatsInd
    
