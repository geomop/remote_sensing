# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 11:03:38 2018

@author: tunabalik
"""

# you can choose from flat, hanning, hamming, bartlett, blackman
def smoothingFilter (xf,window_len,window):
    s=np.r_[xf[window_len-1:0:-1],xf,xf[-2:window_len-1:-1]]
    if window == 'flat':
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')
    yf=np.convolve(w/w.sum(),s,mode='valid')
    yf_cr=yf[0:len(xf)]
    return yf_cr


